/**
The MIT License (MIT)

Copyright (c) 2014 Tristan Edwards

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

function PopUpInstruction() {
  swal({
    title: "Kamera",
    text:
      "Gdy pojawi się w lewym górym rogu kamera - ustaw głowę tak, by mieściła się w ramce a rysy twarzy były obrysowane zieloną linią.",
    buttons: {
      confirm: true
    }
  }).then((isConfirm) =>
    swal({
      title: "Kalibracja",
      text:
        "Naciskaj każdą z widocznych kropek na ekranie. Kliknij każdą po 5 razy, aż kropka zmieni kolor na czarny (spróbuj podążać wzrokiem za kursorem). Ten krok pomoże skalibrować pomiary ruchów oczu."
    })
  );

  console.log("ok");
}

function PopUpCalibrationFinished() {
  swal({
    title: "Zakończono",
    text: "Kalibracja zakończona!",
    icon: "success",
    buttons: {
      confirm: true
    }
  }).then((isConfirm) => {
    PopUpCalibrationMeasurementInfo();
  });
}

function PopUpCalibrationMeasurementInfo() {
  swal({
    title: "Obliczanie pomiaru",
    text:
      "Przez najbliższe 5 sekund trzymaj kursor i wzrok na środkowej kropce, bez ruchu. To umożliwi obliczenie precyzyjności prognoz.",

    closeOnEsc: false,
    allowOutsideClick: false,
    closeModal: true
  }).then((isConfirm) => {
    PopUpCalibrationMeasurementFinished();
  });
}

function PopUpCalibrationMeasurementFinished() {
  // makes the variables true for 5 seconds & plots the points
  $(document).ready(function() {
    storePointsVariable(); // start storing the prediction points

    sleep(5000).then(() => {
      stopStoringPointsVariable(); // stop storing the prediction points
      var past50 = getPoints(); // retrieve the stored points
      var precision_measurement = calculatePrecision(past50);

      console.log(precision_measurement);
      swal({
        title: "Dokładność pomiaru wynosi: " + precision_measurement + "%",
        allowOutsideClick: false,
        buttons: {
          //cancel: "Recalibrate",
          confirm: true
        }
      }).then((isConfirm) => {
        webgazer.end();
        window.location.href = "readingView.html";
      });
    });
  });
}

function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
