let listOfPrediction = [];
let csv = "time,x,y,trial,word\n";
let isReaderReady = false;

window.onload = function() {
  let startTime = Date.now();
  //start the webgazer tracker
  webgazer
    .setRegression("ridge") /* currently must set regression and tracker */
    .setTracker("clmtrackr")
    .setGazeListener(function(data, elapsedTime) {
      if (data == null || isReaderReady === false) {
        return;
      }
      let wordElement = document.elementFromPoint(data.x, data.y);
      if (wordElement == null) {
        return;
      }
      let wordClass = wordElement.className;
      let word = "";
      if (wordClass != null && wordClass == "wordArticle") {
        word = wordElement.innerHTML;
      }

      let dataToSave = data.x + ", " + data.y + "," + word;
      console.log(dataToSave);

      if (typeof data.x == "number" && typeof data.y == "number") {
        let dataRow = [Date.now() - startTime, data.x, data.y, 1, word];
        csv += dataRow.join(",");
        csv += "\n";
      }
    })
    .begin()
    .showPredictionPoints(false);

  //Set up the webgazer video feedback.
  var setup = function() {
    //Set up the main canvas. The main canvas is used to calibrate the webgazer.
    var canvas = document.getElementById("plotting_canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.position = "fixed";
  };

  function checkIfReady() {
    if (webgazer.isReady()) {
      setup();
    } else {
      setTimeout(checkIfReady, 100);
    }
  }
  setTimeout(checkIfReady, 100);
};

function StartReading() {
  $(document).ready(function() {
    setTimeout(function() {
      readingStartInfo();
    }, 1000);
  });
}

function readingStartInfo() {
  swal({
    title: "Gotowy/a?",
    allowOutsideClick: false,
    type: "warning",
    text: "Po zatwierdzeniu tej wiadomości, zacznij czytać :)",
    confirmButtonText: "3, 2, 1, start!",
    buttons: {
      confirm: true
    }
  }).then((isConfirm) => {
    /* if (webgazer != null) */ saveDataToFile();
    //else console.log("webgazer does not work propertly!");
  });
}

function saveDataToFile() {
  let startTime = Date.now();
  isReaderReady = true;
  //starts the data collection that enables the predictions
  // webgazer
  //   .setRegression("ridge")
  //   .setTracker("clmtrackr")
  //   .setGazeListener(function(data, elapsedTime) {
  //     if (data == null || isReaderReady === false) {
  //       return;
  //     }
  //     var dataToSave = data.x + ", " + data.y;
  //     console.log(dataToSave);

  //     if (typeof data.x == "number" && typeof data.y == "number") {
  //       let dataRow = [Date.now() - startTime, data.x, data.y, 1];
  //       csv += dataRow.join(",");
  //       csv += "\n";
  //     }
  //   })
  //   .begin()
  //   .showPredictionPoints(false);
}

function calculateQuizResult() {
  if (!$("#questionsForm").valid()) return;

  let correctValues = [4, 1, 4, 3];
  let result = 0;

  for (let i = 0; i < correctValues.length; i++) {
    let answ = $("input[name=testComprehensionGroup" + i + "]:checked").val();
    if (answ != "undefined" && correctValues[i] == answ) {
      result += 25;
    }
  }
  return result;
}

function finishWebGazerAndSavePredic() {
  webgazer.end();

  var hiddenElement = document.createElement("a");
  hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(csv);
  hiddenElement.target = "_blank";
  hiddenElement.download = "dataDemo.csv";
  hiddenElement.click();
}

window.onbeforeunload = function() {
  window.localStorage.clear(); //Comment out if you want to save data across different sessions
};

let introductionText =
  "Kubuś Puchatek, również Fredzia Phi-Phi (ang. Winnie-the-Pooh) – fikcyjna postać z literatury dziecięcej stworzona przez brytyjskiego pisarza A.A. Milnea. W pierwszej książce o Kubusiu Puchatku wydanej 14 października 1926 roku pojawiły się takie postacie jak: Prosiaczek, Królik, Kłapouchy, Sowa Przemądrzała, Mama Kangurzyca i Maleństwo oraz Krzyś (którego pierwowzorem był syn autora), w drugiej części pt. Chatka Puchatka autor dodatkowo wprowadził postać Tygrysa.";

let nameOriginH = "Pochodzenie imienia";
let nameOriginText =
  "Podobnie jak i wiele innych postaci z książek Milne’a, Kubuś Puchatek został nazwany imieniem jednej z zabawek Christophera Robina Milne’a (1920–1996), syna pisarza. Z kolei pluszowa zabawka Christophera została nazwana tak od imienia niedźwiedzicy Winnipeg będącej żywą maskotką kanadyjskiego wojska z Korpusu Weterynaryjnego Kanady. Została kupiona za 27 dolarów przez porucznika Iana Gary’ego w miasteczku White River w Ontario, gdzie dziś znajduje się pomnik misia. W październiku 1914 roku została przewieziona do Wielkiej Brytanii. Postanowiono pozostawić ją w londyńskim ogrodzie zoologicznym. Padła 12 maja 1934.W 1924 roku Alan Milne pierwszy raz przyszedł do zoo z czteroletnim synem Christopherem, który bardzo polubił Winnie. Trzy lata wcześniej Milne kupił w domu handlowym Harrods i dał synowi na jego pierwsze urodziny pluszaka firmy „Alfa Farnell”. We wrześniu 1981 61-letni Christopher Robin Milne, uczestniczył w odsłonięciu pomnika niedźwiedzicy Winnie (naturalnych rozmiarów, dzieło Lorne Makkina) w londyńskim zoo.";

let polishTranslationH = "Polskie tłumaczenia";
let polishTranslationText =
  "Najstarsze i wciąż najbardziej popularne tłumaczenie obu książek (jak i Wierszy) jest autorstwa Ireny Tuwim. Istnieje też nowszy przekład, pod tytułem Fredzia Phi-Phi, autorstwa Moniki Adamczyk-Garbowskiej (wydawnictwo Lublin 1986, nakład 100 000 egz.) – kontrowersyjny przekład tytułu uważany jest przez jego zwolenników za bliższy oryginałowi.      W tłumaczeniach Kubusia Puchatka na język polski występuje problem określenia jego płci. Irena Tuwim rozumując, że nie da się przełożyć imienia tak, by sprawa jego pochodzenia i rozumienia była jasna dla polskiego czytelnika, ochrzciła misia imieniem Kubuś. Monika Adamczyk we Fredzi Phi-Phi, podobnie jak Milne, nadała imię żeńskie męskiemu misiowi, konsekwentnie stosując formy męskie np. (...) Phi miał zamknięte oczy, Fredzia Phi-Phi zatrzymał się nagle i pochylił się zagadkowo nad śladami. Milne ani razu nie nazywa „Kubusia” „Winnie”, stosuje tylko połączenie „Winnie the Pooh”, a najczęściej nazywa misia po prostu „Pooh”. Używa w odniesieniu do niego męskich zaimków osobowych.";

let extracitialHeroH = "Pozaksiążkowe wcielenia bohatera";
let extracitialHeroText =
  "Firma Disney rozwija wizerunek Kubusia poprzez produkcję filmów animowanych, krótko- i długometrażowych, które mają coraz luźniejszy związek z pierwowzorem literackim oraz oryginalnymi ilustracjami E.H. Sheparda. Oprócz powieści A.A. Milne’a, postać Kubusia wykorzystywana jest też w licznych poradnikach dla dzieci i dorosłych, które starają się tłumaczyć rozmaite złożone problemy jak najprostszym językiem. Ich przykłady to m.in. książka Benjamina Hoffa Tao Kubusia Puchatka i jej kontynuacja Te Prosiaczka, które na przykładzie postaci z cyklu objaśniają założenia filozofii chińskiej.W 2011 roku do kin weszła kolejna produkcja o Kubusiu Puchatku wytwórni Walt Disney Animation Studios pt. Kubuś i przyjaciele, zrealizowana na podstawie wszystkich wydanych do tej pory książek o Puchatku, również na podstawie Powrotu do Stumilowego Lasu autorstwa Davida Benedictusa z 2009 roku.";

let counter = 0;
let counterH = 0;
let introductionSplitted = introductionText.split(" ");
let nameOriginSplitted = nameOriginText.split(" ");
let polishTranslationSplitted = polishTranslationText.split(" ");
let extracitialHeroSplitted = extracitialHeroText.split(" ");

function generateArticle() {
  generateText(introductionSplitted, "#introduction");
  createH3Element(counterH, "#nameOrigin", nameOriginH);
  generateText(nameOriginSplitted, "#nameOrigin");
  createH3Element(++counterH, "#polishTranslation", polishTranslationH);
  generateText(polishTranslationSplitted, "#polishTranslation");
  createH3Element(++counterH, "#extracitialHero", extracitialHeroH);
  generateText(extracitialHeroSplitted, "#extracitialHero");
}

function generateText(textElements, idAppendToName) {
  textElements.forEach(function(entry) {
    createDivElement(counter, idAppendToName, entry);
    counter++;
  });
}

function createDivElement(counter, appendToIdName, entry) {
  $("<div class='wordArticle' id='word" + counter + "'>" + entry + "   </div>").appendTo(
    appendToIdName
  );
}

function createH3Element(counter, appendToIdName, entry) {
  $("<h3 id='headline" + counter + "'>" + entry + " </h3>").appendTo(appendToIdName);
}
